var phonegap_app = phonegap_app || {};

(function($) {
    phonegap_app.HomeView = Backbone.View.extend({
        el : $('#app-view'),
        template : _.template(phonegap_app.templates.home),

        initialize: function(options) {
            //this.model = options.model;
        },

        render : function() {
            //Keep this as an example of how to pass content variables to the template
            //return this.template({content : this.model.content});
            return this.template();
        }
    });
})(jQuery);

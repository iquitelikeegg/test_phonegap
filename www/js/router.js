var phonegap_app = phonegap_app || {};

(function() {
    var Router = Backbone.Router.extend({
        routes : {
            'camera' : 'camera',
            '' : 'home',
            '*notFound' : 'home'

        },

        //Add new routes for the pages

        home : function() {
            var options = {};

            var view = new phonegap_app.HomeView();

            app.view.clearPage();
            app.view.renderToPage(view.render());
        },

        camera : function() {
            var options = {};

            var view = new phonegap_app.CaptureView();

            app.view.clearPage();
            app.view.renderToPage(view.render());
        }

        //go : function(pathname) {
            // if (pathname === null) {
            //     //set custom name for the homepage
            //     pathname = 'home/';
            // }

            //var slug = pathname.substr(0, pathname.length - 1);
            // var url  = '/api/get_page/?slug=';
            //
            // if (!_.isNull(slug)) {
            //     url += slug;
            // }

            // phonegap_app.Pages.url = url;
            //
            // phonegap_app.Pages.fetch({
            //     success: function(model, response, options) {
            //         var viewOptions = {model: response.page};
            //
            //         var rightLayout = ['live', 'past-events'];
            //
            //         if (rightLayout.indexOf(response.page.slug) !== -1) {
            //             viewOptions.right = true;
            //         }
            //
            //         var view = new phonegap_app.PageView(viewOptions);
            //
            //         jQuery('#content').css('opacity', '0');
            //         jQuery('#content').append(view.render());
            //
            //         jQuery('#content').animate(
            //             {'opacity': '1'},
            //             500,
            //             'linear'
            //         );
            //     },
            //     reset: true
            // });
        //},
    });

    phonegap_app.Router = new Router();

    Backbone.history.start({
        pushState: true,
        silent: true
    });
})();

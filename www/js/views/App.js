var phonegap_app = phonegap_app || {};

(function($) {
    phonegap_app.AppView = Backbone.View.extend({
        el: 'body',

        // Bind click events to these selectors.
        events : {
            'click .camera-start' : 'initRouter'
        },

        initialize : function() {
            this.page = $('#app-view');
        },

        initRouter : function(event) {
            event.preventDefault();

            var pathname = event.target.pathname;

            phonegap_app.Router.navigate(pathname, {trigger: true});
        },

        clearPage : function() {
            this.page.html('');
        },

        renderToPage : function(renderedPage) {
            this.page.html(renderedPage);
        }
    });
})(jQuery);

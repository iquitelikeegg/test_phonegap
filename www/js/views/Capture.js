var phonegap_app = phonegap_app || {};

(function($) {
    phonegap_app.CaptureView = Backbone.View.extend({
        el : $('#app-view'),
        template : _.template(phonegap_app.templates.camera),
        cameraHandler : {},

        success : function(one) {
            console.log('success');
            console.log(one);
        },

        fail : function(message) {
            console.log('fail');
            console.log(message);
        },

        initialize: function(options) {
            //this.model = options.model;
            this.cameraHandler = navigator.camera.getPicture(this.success, this.fail, {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                popoverOptions: new CameraPopoverOptions(
                    300, 300, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY
                )
            });

            var self = this;

            window.onorientationchange = function() {
                var cameraPopoverOptions = new CameraPopoverOptions(
                    0, 0, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY
                );
                self.cameraHandler.setPosition(cameraPopoverOptions);
            };
        },

        render : function() {
            return this.template();
        }
    });
})(jQuery);
